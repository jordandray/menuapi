DROP TABLE menu_item_option;
DROP TABLE menu_item;
DROP TABLE menu_category;
DROP TABLE menu_category_item_ref;
DROP TABLE menu_header;

CREATE TABLE menu_header
(
    menu_header_id BIGINT,
    name           VARCHAR(255),
    CONSTRAINT pk_menu_header PRIMARY KEY (menu_header_id)
);

CREATE TABLE menu_category
(
    menu_category_id BIGINT,
    name             VARCHAR(255),
    menu_header_id   BIGINT,
    CONSTRAINT pk_menu_category PRIMARY KEY (menu_category_id)
);

ALTER TABLE menu_category
    ADD CONSTRAINT FK_MENU_CATEGORY_ON_MENU_HEADER FOREIGN KEY (menu_header_id) REFERENCES menu_header (menu_header_id);

CREATE TABLE menu_item
(
    menu_item_id BIGINT,
    base_price   BIGINT,
    name         VARCHAR(255),
    description  VARCHAR(255),
    CONSTRAINT pk_menu_item PRIMARY KEY (menu_item_id)
);

CREATE TABLE menu_item_option
(
    menu_item_option_id   BIGINT,
    menu_item_id          BIGINT,
    base_price_adjustment BIGINT,
    name                  VARCHAR(255),
    description           VARCHAR(255),
    CONSTRAINT pk_menu_item_option PRIMARY KEY (menu_item_option_id)
);

ALTER TABLE menu_item_option
    ADD CONSTRAINT FK_MENU_ITEM_OPTION_ON_MENU_ITEM FOREIGN KEY (menu_item_id) REFERENCES menu_item (menu_item_id);

CREATE TABLE menu_category_item_ref
(
    menu_category_id BIGINT,
    menu_item_id BIGINT,
    menu_header_id BIGINT
);

ALTER TABLE menu_category_item_ref
    ADD CONSTRAINT FK_MENU_CATEGORY_ITEM_REF_ON_MENU_HEADER FOREIGN KEY (menu_header_id) REFERENCES menu_header (menu_header_id);
ALTER TABLE menu_category_item_ref
    ADD CONSTRAINT FK_MENU_CATEGORY_ITEM_REF_ON_MENU_CATEGORY FOREIGN KEY (menu_category_id) REFERENCES menu_category (menu_category_id);
ALTER TABLE menu_category_item_ref
    ADD CONSTRAINT FK_MENU_CATEGORY_ITEM_REF_ON_MENU_ITEM FOREIGN KEY (menu_item_id) REFERENCES menu_item (menu_item_id);


CREATE SEQUENCE public.menu_header_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1
    NO CYCLE;

CREATE SEQUENCE public.menu_category_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1
    NO CYCLE;

CREATE SEQUENCE public.menu_item_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1
    NO CYCLE;

CREATE SEQUENCE public.menu_item_option_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1
    NO CYCLE;

CREATE OR REPLACE VIEW public.menu_item_list_v
AS SELECT mi.menu_item_id,
          mi.name,
          mi.description,
          mi.base_price,
          ( SELECT count(*) AS count
            FROM menu_item_option mio
            WHERE mi.menu_item_id = mio.menu_item_id) AS options_count
   FROM menu_item mi;

CREATE OR REPLACE VIEW public.menu_header_list_v
AS SELECT mh.menu_header_id,
          mh.name
   FROM menu_header mh;

select mh.menu_header_id, mh."name" from menu_item mi join menu_category_item_ref mcir on mi.menu_item_id = mcir.menu_item_id join menu_header mh on mh.menu_header_id = mcir.menu_header_id where mi.menu_item_id = 356;
