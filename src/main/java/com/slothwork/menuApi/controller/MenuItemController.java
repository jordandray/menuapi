package com.slothwork.menuApi.controller;

import com.slothwork.menuApi.model.dto.MenuItemDto;
import com.slothwork.menuApi.model.dto.MenuItemListViewDto;
import com.slothwork.menuApi.service.MenuItemService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/menuItem")
public class MenuItemController {
    private final MenuItemService menuItemService;

    public MenuItemController(MenuItemService menuItemService) {
        this.menuItemService = menuItemService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<MenuItemDto> get(@PathVariable("id") Long id) {
        try {
            ResponseEntity<MenuItemDto> result = null;
            MenuItemDto menuItemDto = menuItemService.getById(id);

            if (menuItemDto != null) {
                result = new ResponseEntity<>(menuItemDto, HttpStatus.OK);
            } else {
                result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return result;
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/list")
    public ResponseEntity<List<MenuItemListViewDto>> getList() {
        try {
            List<MenuItemListViewDto> menuItemListViewDtoList = menuItemService.getItemList();
            return new ResponseEntity<>(menuItemListViewDtoList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping
    public ResponseEntity<MenuItemDto> update(@RequestBody MenuItemDto menuItemDto) {
        try {
            MenuItemDto result = menuItemService.merge(menuItemDto);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        try {
            menuItemService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
