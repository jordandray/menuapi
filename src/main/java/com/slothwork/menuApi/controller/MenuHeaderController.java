package com.slothwork.menuApi.controller;

import com.slothwork.menuApi.model.dto.MenuHeaderDto;
import com.slothwork.menuApi.model.dto.MenuHeaderListViewDto;
import com.slothwork.menuApi.service.MenuHeaderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/menuHeader")
public class MenuHeaderController {
    private final MenuHeaderService menuHeaderService;

    public MenuHeaderController(MenuHeaderService menuHeaderService) {
        this.menuHeaderService = menuHeaderService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<MenuHeaderDto> get(@PathVariable("id") Long id) {
        try {
            ResponseEntity<MenuHeaderDto> result = null;
            MenuHeaderDto menuHeaderDto = menuHeaderService.getById(id);

            if (menuHeaderDto != null) {
                result = new ResponseEntity<>(menuHeaderDto, HttpStatus.OK);
            } else {
                result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return result;
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/list")
    public ResponseEntity<List<MenuHeaderListViewDto>> getList() {
        try {
            List<MenuHeaderListViewDto> menuHeaderListViewDtoList = menuHeaderService.getHeaderList();
            return new ResponseEntity<>(menuHeaderListViewDtoList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/list/menuItem/{menuItemId:[0-9]+}")
    public ResponseEntity<List<MenuHeaderListViewDto>> getListByMenuItemId(@PathVariable Long menuItemId) {
        try {
            List<MenuHeaderListViewDto> menuHeaderListViewDtoList = menuHeaderService.getHeaderListByMenuItemId(menuItemId);
            return new ResponseEntity<>(menuHeaderListViewDtoList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping
    public ResponseEntity<MenuHeaderDto> update(@RequestBody MenuHeaderDto menuHeaderDto) {
        try {
            MenuHeaderDto result = menuHeaderService.merge(menuHeaderDto);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/{id:[0-9]+}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        try {
            menuHeaderService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
