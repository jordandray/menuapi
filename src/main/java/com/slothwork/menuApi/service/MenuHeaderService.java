package com.slothwork.menuApi.service;

import com.slothwork.menuApi.model.dto.MenuHeaderDto;
import com.slothwork.menuApi.model.dto.MenuHeaderListViewDto;
import com.slothwork.menuApi.model.entity.postgres.MenuHeader;
import com.slothwork.menuApi.model.mapper.MenuHeaderListViewMapper;
import com.slothwork.menuApi.model.mapper.MenuHeaderMapper;
import com.slothwork.menuApi.repository.postgres.MenuHeaderListViewRepository;
import com.slothwork.menuApi.repository.postgres.MenuHeaderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuHeaderService {
    // Repositories
    private final MenuHeaderRepository menuHeaderRepository;
    private final MenuHeaderListViewRepository menuHeaderListViewRepository;

    // Mappers
    private final MenuHeaderMapper menuHeaderMapper;
    private final MenuHeaderListViewMapper menuHeaderListViewMapper;

    public MenuHeaderService(
            MenuHeaderRepository menuHeaderRepository,
            MenuHeaderListViewRepository menuHeaderListViewRepository,
            MenuHeaderMapper menuHeaderMapper,
            MenuHeaderListViewMapper menuHeaderListViewMapper) {
        this.menuHeaderRepository = menuHeaderRepository;
        this.menuHeaderListViewRepository = menuHeaderListViewRepository;
        this.menuHeaderMapper = menuHeaderMapper;
        this.menuHeaderListViewMapper = menuHeaderListViewMapper;
    }

    public List<MenuHeaderListViewDto> getHeaderList() {
        return menuHeaderListViewMapper.menuHeaderListViewListToMenuHeaderListViewDtoList(menuHeaderListViewRepository.findAll());
    }

    public List<MenuHeaderListViewDto> getHeaderListByMenuItemId(Long menuItemId) {
        return menuHeaderListViewMapper.menuHeaderListViewListToMenuHeaderListViewDtoList(menuHeaderListViewRepository.menuHeaderListByMenuItemId(menuItemId));
    }

    public MenuHeaderDto getById(Long id) {
        return menuHeaderMapper.menuHeaderToMenuHeaderDto(menuHeaderRepository.findById(id).orElse(null));
    }

    public MenuHeaderDto merge(MenuHeaderDto menuHeaderDto) {
        if (menuHeaderDto.getMenuHeaderId() == null) {
            Long menuHeaderId = menuHeaderRepository.getNextSeq();

            menuHeaderDto.setMenuHeaderId(menuHeaderId);
            menuHeaderDto.getMenuCategories().forEach(mc -> mc.setMenuHeaderId(menuHeaderId));
        }
        MenuHeader menuHeader = menuHeaderMapper.menuHeaderDtoToMenuHeader(menuHeaderDto);
        menuHeaderRepository.saveAndFlush(menuHeader);
        // Fetch entity with updated children and return
        return getById(menuHeader.getMenuHeaderId());
    }

    public void delete(Long id) {
        menuHeaderRepository.deleteById(id);
    }
}
