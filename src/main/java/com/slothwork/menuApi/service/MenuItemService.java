package com.slothwork.menuApi.service;

import com.slothwork.menuApi.model.dto.MenuItemDto;
import com.slothwork.menuApi.model.dto.MenuItemListViewDto;
import com.slothwork.menuApi.model.entity.postgres.MenuItem;
import com.slothwork.menuApi.model.mapper.MenuItemListViewMapper;
import com.slothwork.menuApi.model.mapper.MenuItemMapper;
import com.slothwork.menuApi.repository.postgres.MenuItemListViewRepository;
import com.slothwork.menuApi.repository.postgres.MenuItemRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuItemService {
    // Repositories
    private final MenuItemRepository menuItemRepository;
    private final MenuItemListViewRepository menuItemListViewRepository;

    //Mappers
    private final MenuItemMapper menuItemMapper;
    private final MenuItemListViewMapper menuItemListViewMapper;

    public MenuItemService(
            MenuItemRepository menuItemRepository,
            MenuItemListViewRepository menuItemListViewRepository,
            MenuItemMapper menuItemMapper,
            MenuItemListViewMapper menuItemListViewMapper) {
        this.menuItemRepository = menuItemRepository;
        this.menuItemListViewRepository = menuItemListViewRepository;
        this.menuItemMapper = menuItemMapper;
        this.menuItemListViewMapper = menuItemListViewMapper;
    }

    public List<MenuItemListViewDto> getItemList() {
        return menuItemListViewMapper.menuItemListViewListToMenuItemListViewDtoList(menuItemListViewRepository.findAll());
    }

    public MenuItemDto getById(Long id) { return menuItemMapper.menuItemToMenuItemDto(menuItemRepository.findById(id).orElse(null)); }

    public MenuItemDto merge(MenuItemDto menuItemDto) {
        MenuItem menuItem = menuItemMapper.menuItemDtoToMenuItem(menuItemDto);
        menuItemRepository.saveAndFlush(menuItem);
        return menuItemMapper.menuItemToMenuItemDto(menuItem, menuItemDto.getSequenceNumber());
    }

    public void delete(Long id) { menuItemRepository.deleteById(id); }
}
