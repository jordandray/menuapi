package com.slothwork.menuApi;

import com.slothwork.menuApi.model.mapper.MenuHeaderListViewMapper;
import com.slothwork.menuApi.model.mapper.MenuHeaderMapper;
import com.slothwork.menuApi.model.mapper.MenuItemListViewMapper;
import com.slothwork.menuApi.model.mapper.MenuItemMapper;
import org.mapstruct.factory.Mappers;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfiguration {
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public MenuHeaderMapper menuHeaderMapper() { return Mappers.getMapper(MenuHeaderMapper.class); }

    @Bean
    public MenuHeaderListViewMapper menuHeaderListViewMapper() { return Mappers.getMapper(MenuHeaderListViewMapper.class); }

    @Bean
    public MenuItemMapper menuItemMapper() { return Mappers.getMapper(MenuItemMapper.class); }

    @Bean
    public MenuItemListViewMapper menuItemListViewMapper() { return Mappers.getMapper(MenuItemListViewMapper.class); }
}
