package com.slothwork.menuApi.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MenuCategoryDto {
    private Long menuCategoryId;
    private String name;
    private Long menuHeaderId;
    private Long sequenceNumber;
    private Set<MenuItemDto> menuItems;
}
