package com.slothwork.menuApi.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MenuHeaderListViewDto {
    private Long menuHeaderId;
    private String name;
}
