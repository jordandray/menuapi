package com.slothwork.menuApi.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MenuItemOptionDto {
    private Long menuItemOptionId;
    private Long menuItemId;
    private Long basePriceAdjustment;
    private String name;
    private String description;
}
