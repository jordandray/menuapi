package com.slothwork.menuApi.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MenuHeaderDto {
    private Long menuHeaderId;
    private String name;
    private Set<MenuCategoryDto> menuCategories;
}
