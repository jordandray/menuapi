package com.slothwork.menuApi.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MenuItemListViewDto {
    private Long menuItemId;
    private String name;
    private String description;
    private Long basePrice;
    private Long optionsCount;
}
