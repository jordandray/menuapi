package com.slothwork.menuApi.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MenuItemDto {
    private Long menuItemId;
    private Long basePrice;
    private String name;
    private String description;
    private Long sequenceNumber;
    private Set<MenuItemOptionDto> menuItemOptions;
}
