package com.slothwork.menuApi.model.mapper;

import com.slothwork.menuApi.model.dto.MenuCategoryDto;
import com.slothwork.menuApi.model.dto.MenuHeaderDto;
import com.slothwork.menuApi.model.dto.MenuItemDto;
import com.slothwork.menuApi.model.entity.postgres.MenuCategory;
import com.slothwork.menuApi.model.entity.postgres.MenuCategoryItemRef;
import com.slothwork.menuApi.model.entity.postgres.MenuHeader;
import org.mapstruct.*;

import java.util.Set;

@Mapper
public interface MenuHeaderMapper {
    MenuHeaderDto menuHeaderToMenuHeaderDto(MenuHeader menuHeader);
    MenuHeader menuHeaderDtoToMenuHeader(MenuHeaderDto menuHeaderDto);

    @Mappings({
            @Mapping(target = "menuItems", source = "entity.menuCategoryItemRefs")
    })
    MenuCategoryDto menuCategoryToMenuCategoryDto(MenuCategory entity);

    @Mappings(
            @Mapping(target = "menuCategoryItemRefs", source = "dto.menuItems")
    )
    MenuCategory menuCategoryDtoToMenuCategory(MenuCategoryDto dto);

    Set<MenuItemDto> menuCategoryItemRefSetToMenuItemDtoSet(Set<MenuCategoryItemRef> menuCategoryItemRefs);
    @Mappings({
            @Mapping(target = "menuItemId", source = "menuCategoryItemRef.menuItem.menuItemId"),
            @Mapping(target = "basePrice", source = "menuCategoryItemRef.menuItem.basePrice"),
            @Mapping(target = "name", source = "menuCategoryItemRef.menuItem.name"),
            @Mapping(target = "description", source = "menuCategoryItemRef.menuItem.description"),
            @Mapping(target = "menuItemOptions", source = "menuCategoryItemRef.menuItem.menuItemOptions")
    })
    MenuItemDto menuCategoryItemRefToMenuItemDto(MenuCategoryItemRef menuCategoryItemRef);

    Set<MenuCategoryItemRef> menuItemDtoSetToMenuCategoryItemRefSet(Set<MenuItemDto> menuItemDtos);
    @Mappings({
            @Mapping(target = "menuItem.menuItemId", source = "menuItemDto.menuItemId"),
            @Mapping(target = "menuItem.basePrice", source = "menuItemDto.basePrice"),
            @Mapping(target = "menuItem.name", source = "menuItemDto.name"),
            @Mapping(target = "menuItem.description", source = "menuItemDto.description"),
            @Mapping(target = "menuItem.menuItemOptions", source = "menuItemDto.menuItemOptions"),
            @Mapping(target = "id.menuItemId", source = "menuItemDto.menuItemId"),
            @Mapping(target = "sequenceNumber", source = "menuItemDto.sequenceNumber"),
    })
    MenuCategoryItemRef menuItemDtoToMenuCategoryItemRef(MenuItemDto menuItemDto);

    @AfterMapping
    default void afterMenuCategoryItemRefMapping(@MappingTarget MenuCategory menuCategory, MenuCategoryDto menuCategoryDto) {
        menuCategory.getMenuCategoryItemRefs().forEach(menuCategoryItemRef -> {
            menuCategoryItemRef.getId().setMenuCategoryId(menuCategoryDto.getMenuCategoryId());
            menuCategoryItemRef.setMenuHeaderId(menuCategoryDto.getMenuHeaderId());
            menuCategoryItemRef.setMenuCategory(menuCategory);
        });
    }
}
