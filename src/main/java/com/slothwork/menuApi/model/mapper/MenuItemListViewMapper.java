package com.slothwork.menuApi.model.mapper;

import com.slothwork.menuApi.model.dto.MenuItemListViewDto;
import com.slothwork.menuApi.model.entity.postgres.MenuItemListView;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface MenuItemListViewMapper {
    MenuItemListViewDto menuItemListViewToMenuItemListViewDto(MenuItemListView menuItemListView);
    List<MenuItemListViewDto> menuItemListViewListToMenuItemListViewDtoList(List<MenuItemListView> menuItemListViews);
}
