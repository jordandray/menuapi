package com.slothwork.menuApi.model.mapper;

import com.slothwork.menuApi.model.dto.MenuHeaderListViewDto;
import com.slothwork.menuApi.model.entity.postgres.MenuHeaderListView;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface MenuHeaderListViewMapper {
    MenuHeaderListViewDto menuHeaderListViewToMenuHeaderListViewDto(MenuHeaderListView menuHeaderListView);
    List<MenuHeaderListViewDto> menuHeaderListViewListToMenuHeaderListViewDtoList(List<MenuHeaderListView> menuHeaderListViews);
}
