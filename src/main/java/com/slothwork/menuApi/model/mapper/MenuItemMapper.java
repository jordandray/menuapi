package com.slothwork.menuApi.model.mapper;

import com.slothwork.menuApi.model.dto.MenuItemDto;
import com.slothwork.menuApi.model.entity.postgres.MenuItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface MenuItemMapper {

    MenuItemDto menuItemToMenuItemDto(MenuItem menuItem);
    @Mappings({
            @Mapping(target = "sequenceNumber", source = "sequenceNumber")
    })
    MenuItemDto menuItemToMenuItemDto(MenuItem menuItem, Long sequenceNumber);
    MenuItem menuItemDtoToMenuItem(MenuItemDto menuItemDto);
}
