package com.slothwork.menuApi.model.entity.postgres;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Table(name = "menu_header")
@Entity
@Getter
@Setter
public class MenuHeader implements Serializable {
    @Id
    @Column(name = "menu_header_id")
    private Long menuHeaderId;

    @Column(name = "name")
    private String name;

    @OneToMany(targetEntity = MenuCategory.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "menu_header_id")
    private Set<MenuCategory> menuCategories;
}
