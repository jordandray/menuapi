package com.slothwork.menuApi.model.entity.postgres;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Table(name = "menu_category")
@Entity
@Getter
@Setter
public class MenuCategory implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "menu_category_seq")
    @SequenceGenerator(name = "menu_category_seq", sequenceName = "menu_category_seq", allocationSize = 1)
    @Column(name = "menu_category_id")
    private Long menuCategoryId;

    @Column(name = "name")
    private String name;

    @Column(name = "menu_header_id")
    private Long menuHeaderId;

    @Column(name = "sequence_number")
    private Long sequenceNumber;

    @OneToMany(mappedBy = "menuCategory", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
    private Set<MenuCategoryItemRef> menuCategoryItemRefs = new HashSet<>();
}
