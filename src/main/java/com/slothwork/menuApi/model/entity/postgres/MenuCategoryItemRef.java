package com.slothwork.menuApi.model.entity.postgres;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "menu_category_item_ref")
@JsonIgnoreProperties(value = {"menuCategory"})
@Entity
@Getter
@Setter
public class MenuCategoryItemRef {
    @EmbeddedId
    private MenuCategoryItemRefId id = new MenuCategoryItemRefId();

    @ManyToOne
    @MapsId("menuCategoryId")
    @JoinColumn(name = "menu_category_id")
    private MenuCategory menuCategory;

    @ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @MapsId("menuItemId")
    @JoinColumn(name = "menu_item_id")
    private MenuItem menuItem;

    @Column(name = "menu_header_id")
    private Long menuHeaderId;

    @Column(name = "sequence_number")
    private Long sequenceNumber;
}
