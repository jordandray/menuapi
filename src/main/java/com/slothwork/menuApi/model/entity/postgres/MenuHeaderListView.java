package com.slothwork.menuApi.model.entity.postgres;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "menu_header_list_v")
@Entity
@Immutable
@Getter
@Setter
public class MenuHeaderListView {
    @Id
    @Column(name = "menu_header_id")
    private Long menuHeaderId;

    @Column(name = "name")
    private String name;
}
