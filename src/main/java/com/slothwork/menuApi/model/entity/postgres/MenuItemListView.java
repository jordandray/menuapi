package com.slothwork.menuApi.model.entity.postgres;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "menu_item_list_v")
@Entity
@Immutable
@Getter
@Setter
public class MenuItemListView {
    @Id
    @Column(name = "menu_item_id")
    private Long menuItemId;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "base_price")
    private Long basePrice;

    @Column(name = "options_count")
    private Long optionsCount;
}
