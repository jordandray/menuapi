package com.slothwork.menuApi.model.entity.postgres;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "menu_item_option")
@Entity
@Getter
@Setter
public class MenuItemOption implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "menu_item_option_seq")
    @SequenceGenerator(name = "menu_item_option_seq", sequenceName = "menu_item_option_seq", allocationSize = 1)
    @Column(name = "menu_item_option_id")
    private Long menuItemOptionId;

    @Column(name = "menu_item_id")
    private Long menuItemId;

    @Column(name = "base_price_adjustment")
    private Long basePriceAdjustment;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;
}
