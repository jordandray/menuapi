package com.slothwork.menuApi.model.entity.postgres;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
public class MenuCategoryItemRefId implements Serializable {
    @Column(name = "menu_category_id")
    private Long menuCategoryId;

    @Column(name = "menu_item_id")
    private Long menuItemId;

    public MenuCategoryItemRefId() { }

    public MenuCategoryItemRefId(Long menuCategoryId, Long menuItemId) {
        super();
        this.menuCategoryId = menuCategoryId;
        this.menuItemId = menuItemId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((menuCategoryId == null) ? 0 : menuCategoryId.hashCode());
        result = prime * result
                + ((menuItemId == null) ? 0 : menuItemId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MenuCategoryItemRefId other = (MenuCategoryItemRefId) obj;
        return Objects.equals(getMenuCategoryId(), other.getMenuCategoryId()) && Objects.equals(getMenuItemId(), other.getMenuItemId());
    }
}
