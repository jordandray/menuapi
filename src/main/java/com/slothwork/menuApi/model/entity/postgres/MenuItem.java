package com.slothwork.menuApi.model.entity.postgres;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Table(name = "menu_item")
@Entity
@Getter
@Setter
@JsonIgnoreProperties({ "menuCategoryItemRefs" })
public class MenuItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "menu_item_seq")
    @SequenceGenerator(name = "menu_item_seq", sequenceName = "menu_item_seq", allocationSize = 1)
    @Column(name = "menu_item_id")
    private Long menuItemId;

    @Column(name = "base_price")
    private Long basePrice;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "menu_item_id")
    private Set<MenuItemOption> menuItemOptions;

    @OneToMany(mappedBy = "menuItem", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
    private Set<MenuCategoryItemRef> menuCategoryItemRefs = new HashSet<>();
}
