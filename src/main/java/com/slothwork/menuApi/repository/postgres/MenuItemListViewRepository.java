package com.slothwork.menuApi.repository.postgres;

import com.slothwork.menuApi.model.entity.postgres.MenuItemListView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuItemListViewRepository extends JpaRepository<MenuItemListView, Long> {
}
