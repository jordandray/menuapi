package com.slothwork.menuApi.repository.postgres;

import com.slothwork.menuApi.model.entity.postgres.MenuItemOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuItemOptionRepository extends JpaRepository<MenuItemOption, Long> {
}
