package com.slothwork.menuApi.repository.postgres;

import com.slothwork.menuApi.model.entity.postgres.MenuHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuHeaderRepository extends JpaRepository<MenuHeader, Long> {
    @Query(value = "select nextval('menu_header_seq')", nativeQuery = true)
    Long getNextSeq();
}
