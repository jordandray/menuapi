package com.slothwork.menuApi.repository.postgres;

import com.slothwork.menuApi.model.entity.postgres.MenuHeaderListView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuHeaderListViewRepository extends JpaRepository<MenuHeaderListView, Long> {
    @Query(
            value = "select mh.menu_header_id, mh.name from menu_item mi join menu_category_item_ref mcir on mi.menu_item_id = mcir.menu_item_id join menu_header mh on mh.menu_header_id = mcir.menu_header_id where mi.menu_item_id = ?1",
            nativeQuery = true
    )
    List<MenuHeaderListView> menuHeaderListByMenuItemId(Long menuHeaderId);
}
