package com.slothwork.menuApi.repository.postgres;

import com.slothwork.menuApi.model.entity.postgres.MenuCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuCategoryRepository extends JpaRepository<MenuCategory, Long> {
}
